from PIL import ImageGrab
import time
import base64
import requests
import json

def getFileName():
    t = time.localtime()
    timestamp = time.strftime('%b-%d-%Y_%H%M%S', t)
    print(timestamp)
    return "screenshot_" + timestamp + '.png'
    
def feedModel(save_path='screenshot.png'):
    t = ''
    while (True):
        screen_filename = getFileName()
        # Capture the screen
        screenshot = ImageGrab.grab()
        # Save the screenshot
        screenshot.save(screen_filename)
        time.sleep(5)
        sendScreenShot(screen_filename)

def sendScreenShot(filename):
    with open(filename, 'rb') as image_file:
        encoded_image = base64.b64encode(image_file.read()).decode('utf-8')

    # Create the JSON payload
    payload = json.dumps({'image': encoded_image})

    # URL of the endpoint
    url = 'https://europe-west1-shift-aihack-nantes24-2.cloudfunctions.net/log_activity'

    # Set the headers
    headers = {
        'Content-Type': 'application/json'
    }

    # Send the POST request
    response = requests.post(url, headers=headers, data=payload, timeout=70)

    # Print the response
    print(response.status_code)
    print(response.text)


feedModel()